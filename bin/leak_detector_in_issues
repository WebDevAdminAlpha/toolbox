#!/usr/bin/env ruby
# frozen_string_literal: true

require 'bundler/setup'
require 'optparse'
require 'gitlab'

require File.expand_path('../lib/token_finder.rb', __dir__)

Options = Struct.new(
  :token,
  :project,
  :needle,
  :api_endpoint,
  :title_filter
)

class LeakDetectorInIssuesOptionParser
  class << self
    def parse(argv)
      options = Options.new

      parser = OptionParser.new do |opts|
        opts.banner = "Usage: #{__FILE__} [options]\n\n"

        opts.on('-t', '--token ACESS_TOKEN', String, 'A valid access token') do |value|
          options.token = value
        end

        opts.on('-p', '--project PROJECT_ID', String, 'A valid project ID. Can be an integer or a group/project string') do |value|
          options.project = value
        end

        opts.on('-n', '--needle NEEDLE', String, 'A string to search') do |value|
          options.needle = value
        end

        opts.on('-a', '--api-endpoint API_ENDPOINT', String, 'A valid GitLab API endpoint, defaults to https://gitlab.com/api/v4') do |value|
          options.api_endpoint = value
        end

        opts.on('-f', '--title-filter TITLE_FILTER', String, 'Only search issues for which title match this filter') do |value|
          options.title_filter = value
        end

        opts.on('-h', '--help', 'Print help message') do
          $stdout.puts opts
          exit
        end
      end
      parser.parse!(argv)

      options
    end
  end
end

class LeakDetectorInIssues
  DEFAULT_API_ENDPOINT = 'https://gitlab.com/api/v4'.freeze

  attr_reader :token, :project, :api_endpoint, :title_filter
  attr_reader :needle, :needle_pattern, :leaking_urls

  def initialize(options)
    @token = TokenFinder.find_token!(options.token)
    @project = options.project
    @api_endpoint = options.api_endpoint || DEFAULT_API_ENDPOINT
    @title_filter = options.title_filter
    @needle = options.needle
    @needle_pattern = Regexp.new(Regexp.escape(needle), Regexp::IGNORECASE)

    assert_project!
    init_gitlab!
  end

  def display_report
    if leaking_urls.any?
      puts "\n#{leaking_urls.size} occurrences of '#{needle}' leaked! 😱\n\n"
      leaking_urls.each do |leaking_url|
        puts "- #{leaking_url}"
      end
    else
      puts "\n\nNo leaked '#{needle}'! 😅"
    end
  end

  private

  def init_gitlab!
    Gitlab.configure do |config|
      config.endpoint = api_endpoint
      config.private_token = token
    end
  end

  def assert_project!
    return if project

    $stderr.puts 'Please provide a valid project ID with the `-p/--project` option!'
    exit 1
  end

  def leaking_urls
    @leaking_urls ||= begin
      Gitlab.issues(project, search: title_filter, per_page: 100).auto_paginate.each_with_object([]) do |issue, memo|
        next if title_filter && issue.title !~ /#{title_filter}/i

        puts "Checking '#{issue.title}'..."

        memo << issue.web_url if detect_in_text(issue.title)
        memo << issue.web_url if detect_in_text(issue.body)

        Gitlab.issue_notes(project, issue.iid, per_page: 100).auto_paginate do |note|
          next if note.system

          memo << "#{issue.web_url}#note_#{note.id}" if detect_in_text(note.body)
        end
      end
    end
  end

  def detect_in_text(text)
    !!(text =~ needle_pattern)
  end
end

if $0 == __FILE__
  start = Time.now
  detector = LeakDetectorInIssues.new(LeakDetectorInIssuesOptionParser.parse(ARGV))
  detector.display_report

  puts "\nDone in #{Time.now - start} seconds."
end
